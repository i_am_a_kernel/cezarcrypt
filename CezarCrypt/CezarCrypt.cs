﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CezarCrypt
{
    public static class CezarCrypt
    {
        public static string Encrypt(string text, uint shift)
        {
            string result = "";
            for (int i = 0; i < text.Length; i++)
            {
                //Если не кирилица
                if(((int)(text[i])<1040)||((int)(text[i])>1103))
                    result +=text[i];
                //Если буква является строчной
                if((Convert.ToInt16(text[i])>=1072)&&(Convert.ToInt16(text[i])<=1103))
                {
                    //Если после сдвига выходит за пределы алфавита
                    if (Convert.ToInt16(text[i]) + shift > 1103)
                        result += Convert.ToChar(Convert.ToInt16(text[i]) + shift - 32);
                    //Если можно сдвигать в пределах алфавита
                    else
                        result += Convert.ToChar(Convert.ToInt16(text[i])+shift);
                }
                //Если буква является прописной
                if ((Convert.ToInt16(text[i]) >= 1040) && (Convert.ToInt16(text[i]) <= 1071))
                {
                    //Если буква, после сдвига выходит за пределы алфавита
                    if (Convert.ToInt16(text[i]) + shift > 1071)
                        //Добавление в строку результатов символ
                        result += Convert.ToChar(Convert.ToInt16(text[i]) + shift - 32);
                    //Если буква может быть сдвинута в пределах алфавита
                    else
                        //Добавление в строку результатов символ
                        result += Convert.ToChar(Convert.ToInt16(text[i]) + shift);
                }
            }
            return result;
        }

        public static string Dectypt(string text, uint shift)
        {
            string result = "";
            for (int i = 0; i < text.Length; i++)
            {
                if (Convert.ToInt16(text[i]) == 32)
                    result += ' ';
                //Если буква является строчной
                if ((Convert.ToInt16(text[i]) >= 1072) && (Convert.ToInt16(text[i]) <= 1103))
                {
                    //Если буква, после сдвига выходит за пределы алфавита
                    if (Convert.ToInt16(text[i]) - shift < 1072)
                        //Добавление в строку результатов символ
                        result += Convert.ToChar(Convert.ToInt16(text[i]) - shift + 32);
                    //Если буква может быть сдвинута в пределах алфавита
                    else
                        //Добавление в строку результатов символ
                        result += Convert.ToChar(Convert.ToInt16(text[i]) - shift);
                }
                //Если буква является прописной
                if ((Convert.ToInt16(text[i]) >= 1040) && (Convert.ToInt16(text[i]) <= 1071))
                {
                    //Если буква, после сдвига выходит за пределы алфавита
                    if (Convert.ToInt16(text[i]) - shift < 1040)
                        //Добавление в строку результатов символ
                        result += Convert.ToChar(Convert.ToInt16(text[i]) - shift + 32);
                    //Если буква может быть сдвинута в пределах алфавита
                    else
                        //Добавление в строку результатов символ
                        result += Convert.ToChar(Convert.ToInt16(text[i]) - shift);
                }
            }
            return result;
        }
    }
}
