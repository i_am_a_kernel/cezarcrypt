﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using CezarCrypt;

namespace CezarConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            do
            {
                string path = "";
                uint k = 0;
                uint z = 0;
                uint shift;
                Console.WriteLine("--- Система шифрования Cezar ---");
                Console.WriteLine("Введите 1 для шифрования или 2 для дешифрования текста");
                while ((k != 1) && (k != 2))
                {
                    uint.TryParse(Console.ReadLine(), out k);
                    if ((k != 1) && (k != 2))
                        Console.WriteLine("Ошибка ввода, повторите попытку");
                }
                Console.WriteLine("Введите 1 для ввода данных в консоль или 2 для ввода из файла");
                while ((z != 1) && (z != 2))
                {
                    uint.TryParse(Console.ReadLine(), out z);
                    if ((z != 1) && (z != 2))
                        Console.WriteLine("Ошибка ввода, повторите попытку");
                }

                if (z == 1)
                {
                    Console.WriteLine("Введите текст с которым необходимо работать:");
                    path = Console.ReadLine();
                    Console.WriteLine("Введите величину сдвига для шифрования/дешифрования:");
                    while (!uint.TryParse(Console.ReadLine(), out shift))
                    {
                        Console.WriteLine("Ошибка ввода, повторите попытку");
                    }

                    if (k == 1)
                    {
                        string resultString = CezarCrypt.CezarCrypt.Encrypt(path, shift);
                        Console.WriteLine(String.Format("Результат шифрования: {0}", resultString));
                    }
                    if (k == 2)
                    {
                        string resultString = CezarCrypt.CezarCrypt.Dectypt(path, shift);
                        Console.WriteLine(String.Format("Результат дешифрования: {0}", resultString));
                    }
                }


                if (z == 2)
                {
                    Console.WriteLine("Введите полный путь к файлу с текстом с которым необходимо работать:");
                    path = Console.ReadLine();
                    Console.WriteLine("Введите величину сдвига для шифрования/дешифрования:");
                    while (!uint.TryParse(Console.ReadLine(), out shift))
                    {
                        Console.WriteLine("Ошибка ввода, повторите попытку");
                    }
                    string tmp = String.Format(@"{0}\{1}{2}",
                        Path.GetDirectoryName(path),
                        String.Concat(Path.GetFileNameWithoutExtension(path), "_new"),
                        Path.GetExtension(path));
                    if (k == 1)
                    {
                        string resultString = CezarCrypt.CezarCrypt.Encrypt(File.ReadAllText(path, Encoding.Default), shift);
                        File.WriteAllText(tmp, resultString, Encoding.Default);
                        Console.WriteLine(String.Format("Результат шифрования сохранен в файл: {0}", tmp));
                    }
                    if (k == 2)
                    {
                        string resultString = CezarCrypt.CezarCrypt.Dectypt(File.ReadAllText(path, Encoding.Default), shift);
                        File.WriteAllText(tmp, resultString, Encoding.Default);
                        Console.WriteLine(String.Format("Результат дешифрования сохранен в файл: {0}", tmp));
                    }
                }
                Console.WriteLine("Для выхода из программы нажмите Escape или любую кнопку для продолжения.");
            }
            while (Console.ReadKey(true).Key != ConsoleKey.Escape);
        }
    }
}
